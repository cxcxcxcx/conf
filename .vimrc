syn on
set et
set ts=4
set sw=2
set encoding=utf8
set fileencoding=utf-8
set fileencodings=ucs-bom,utf-8,gbk,gb18030,default
set guifont=DejaVu\ Sans\ Mono\ 12
"set cindent

set nobackup

filetype plugin indent on
autocmd FileType python,pyrex setlocal et sta sw=4 sts=4
autocmd FileType python setlocal foldmethod=indent foldlevel=99

autocmd FileType htmldjango,html,javascript setlocal et sta sw=2 sts=2
autocmd FileType cpp setlocal et sta sw=2 sts=2 ts=2

" Break line at Chinese characters if needed.
set formatoptions+=mB

" Automatically wrap lines for latex files.
autocmd FileType tex,plaintex setlocal et sta sts=2 sw=2 ts=2
autocmd FileType tex,plaintex setlocal textwidth=80 formatoptions+=tcqn
map \gq ?^$\\|^\s*\(\\begin\\|\\end\\|\\label\)?1<CR>gq//-1<CR>
omap lp ?^$\\|^\s*\(\\begin\\|\\end\\|\\label\)?1<CR>//-1<CR>.<CR>
autocmd FileType tex,plaintex let &l:flp = '^\s*\\\ze\(begin\|end\|item\)\>'
let g:tex_flavor='latex'
autocmd FileType tex,plaintex setlocal iskeyword+=:

au BufNewFile,BufRead *.mak set filetype=mako

"let mapleader = ","
set tags=tags;/

" :gl to show blame
vmap gl :<C-U>!svn blame <C-R>=expand("%:p") <CR> \| sed -n <C-R>=line("'<") <CR>,<C-R>=line("'>") <CR>p <CR>


set ruler                           " show line and column number
set showcmd                     " show (partial) command in status line
set background=dark

if has('gui_running')
	colorscheme wombat
endif

nnoremap <c-]> g<c-]>
vnoremap <c-]> g<c-]>

nnoremap g<c-]> <c-]>
vnoremap g<c-]> <c-]>

map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
map <A-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>

set completeopt+=longest

inoremap <expr> <CR>       pumvisible() ? "\<C-y>" : "\<CR>"
inoremap <expr> <tab>       pumvisible() ? "\<C-y>" : "\<TAB>"
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

""For autocomplete
let g:acp_completeoptPreview = 1

" --------------------
" TagList
" --------------------
"" F4: Switch on/off TagList
"nnoremap <silent> <F4> :TlistToggle<CR>
"let Tlist_Show_One_File = 1 " Displaying tags for only one file~
let Tlist_Exist_OnlyWindow = 1 " if you are the last, kill yourself
"let Tlist_Use_Right_Window = 1 " split to the right side of the screen
let Tlist_Sort_Type = "order" " sort by order or name
let Tlist_Display_Prototype = 0 " do not show prototypes and not tags in the taglist window.
let Tlist_Compart_Format = 1 " Remove extra information and blank lines from the taglist window.
let Tlist_GainFocus_On_ToggleOpen = 1 " Jump to taglist window on open.
let Tlist_Display_Tag_Scope = 1 " Show tag scope next to the tag name.
"let Tlist_Close_On_Select = 1 " Close the taglist window when a file or tag is selected.
let Tlist_Enable_Fold_Column = 0 " Don't Show the fold indicator column in the taglist window.
let Tlist_WinWidth = 20
" let Tlist_Ctags_Cmd = 'ctags --c++-kinds=+p --fields=+iaS --extra=+q --languages=c++'
" very slow, so I disable this
" let Tlist_Process_File_Always = 1 " To use the :TlistShowTag and the :TlistShowPrototype commands without the taglist window and the taglist menu, you should set this variable to 1.
":TlistShowPrototype [filename] [linenumber]


let python_highlight_all=1
map <F3> za

"显示行号：
set number
"为方便复制，用<F2>开启/关闭行号显示:
nnoremap <F2> :set nonumber!<CR>:set foldcolumn=0<CR>

"Remove trailing spaces.
autocmd FileType c,cpp,java,php,python autocmd BufWritePre <buffer> :call setline(1,map(getline(1,"$"),'substitute(v:val,"\\s\\+$","","")'))

let g:winManagerWindowLayout='FileExplorer|TagList'
nnoremap <silent> <F4> :WMToggle<CR>

"let g:pep8_map='<F6>'
" I'm using flakes8 for quickfix. 
let g:pyflakes_use_quickfix = 0

set mouse=a

" Vundle
set nocompatible               " be iMproved
filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#begin()
"call vundle#rc()

" let Vundle manage Vundle
" required! 
Plugin 'gmarik/vundle'

" My Plugins here:
"
" original repos on github
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'tpope/vim-fugitive'
Plugin 'Lokaltog/vim-powerline'
Plugin 'majutsushi/tagbar'
Plugin 'chikamichi/mediawiki.vim'

Plugin 'nvie/vim-flake8'
Plugin 'jcf/vim-latex'
Plugin 'kchmck/vim-coffee-script'
Plugin 'groenewege/vim-less'

Plugin 'sophacles/vim-bundle-mako'
Plugin 'rhysd/vim-clang-format'
Plugin 'chr4/nginx.vim'

Plugin 'Quramy/tsuquyomi'
Plugin 'leafgarland/typescript-vim'
Plugin 'peitalin/vim-jsx-typescript'
Plugin 'othree/yajs.vim'

Plugin 'mindriot101/vim-yapf'
Plugin 'vhda/verilog_systemverilog.vim'
Plugin 'vim-scripts/a.vim'

Plugin 'bazelbuild/vim-ft-bzl'

"Plugin 'Lokaltog/vim-easymotion'
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
"Plugin 'tpope/vim-rails.git'
" vim-scripts repos
"Plugin 'L9'
"Plugin 'FuzzyFinder'
"" non github repos
"Plugin 'git://git.wincent.com/command-t.git'
" ...
call vundle#end()

filetype plugin indent on     " required!


set nocompatible
set laststatus=2
"let g:Powerline_symbols = 'unicode'
let g:Powerline_symbols = 'fancy'

"call pathogen#infect()
set t_Co=256 " Explicitly tell vim that the terminal supports 256 colors

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
let g:NERDDefaultAlign = 'left'

let g:black_linelength = 79
autocmd FileType verilog_systemverilog setlocal formatprg=verible-verilog-format\ -
